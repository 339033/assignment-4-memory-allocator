#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>

#define REGION_SIZE 4096
#define heap_size1 100
#define heap_size2 100
const int a=342, b=99,c=55;

static struct block_header* block_get_header(void* block)
{
    return (struct block_header*) (((uint8_t*)block) - offsetof(struct block_header, contents));
}

static void test1_mallocMemoryAccess()
{
    printf("Test1, malloc memory access - \n");

    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap != NULL);

    void* content = _malloc(16);
    struct block_header* header = block_get_header(content);
    assert(header->is_free == false);
    assert(header->capacity.bytes == 24);

    heap_term();
    printf("Test 1 - Successfully done\n\n");
}

static void test2_blockFree()
{
    printf("Test 2, block is free testing\n");

    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap != NULL);

    void* content1 = _malloc(heap_size1);
    assert(content1 != NULL);

    void* content2 = _malloc(heap_size2);
    assert(content2 != NULL);

    _free(content2);

    struct block_header* header1 = block_get_header(content1);
    struct block_header* header2 = block_get_header(content2);

    _free(content1);
    assert(header1->is_free);
    assert(header2->is_free);
    assert(header1->next == header2);

    heap_term();

    printf("Test 2 successfully done.\n\n");
}

static void test3_blockMultipleFree()
{
    printf("Test 3, free multiple testing.\n");

    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap != NULL);

    void* content1 = _malloc(a);
    assert(content1 != NULL);

    void* content2 = _malloc(a);
    assert(content2 != NULL);

    void* content3 = _malloc(b);
    assert(content3 != NULL);

    void* content4 = _malloc(c);
    assert(content4 != NULL);

    _free(content2);
    _free(content4);

    struct block_header* header1 = block_get_header(content1);
    struct block_header* header2 = block_get_header(content2);
    struct block_header* header3 = block_get_header(content3);
    struct block_header* header4 = block_get_header(content4);

    assert(header1->next == header2);

    assert(header2->is_free);

    assert(header3->next == header4);

    assert(header4->is_free);

    heap_term();

    printf("Test 3 successfully done.\n\n");
}

static void test4_growHeapNewRegion()
{
    printf("Test 4, new region grow heap testing\n");

    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap != NULL);

    void* content = _malloc(REGION_MIN_SIZE);
    assert(content != NULL);

    struct block_header* header = block_get_header(content);
    assert(header->capacity.bytes == REGION_MIN_SIZE);


    heap_term();

    printf("Test 4 successfully done.\n\n");
}

static void test5_anotherPlaceSeparated() {
    printf("Test 5, another place/separated grow heap testing\n");

    void* heap = heap_init(0);

    void* content = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    void* space = mmap(HEAP_START + REGION_MIN_SIZE, 24, 0, MAP_PRIVATE | 0x20, -1, 0);

    assert(heap);
    assert(content);
    assert(space);

    void* separate_content = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    assert(content);
    assert(separate_content);

    struct block_header* separate_header = block_get_header(separate_content);
    munmap(space, 24);

    struct block_header* header = (struct block_header*) heap;

    assert(header->next == separate_header);
    assert((void*) header + offsetof(struct block_header, contents) + header->capacity.bytes != separate_header);




    heap_term();

    printf("Test 5 successfully done.\n\n");
}


int main()
{
    test1_mallocMemoryAccess();
    test2_blockFree();
    test3_blockMultipleFree();
    test4_growHeapNewRegion();
    test5_anotherPlaceSeparated();

    return 0;
}